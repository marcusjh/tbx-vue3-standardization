// Modules
import { createApp } from 'vue';
import { createRouter, createWebHistory } from 'vue-router';
import { createI18n } from 'vue-i18n';

// Components
import App from './App.vue';
import Modules from './components/examples/Modules.vue';
import Home from './components/examples/Home.vue';
import Suspense from './components/examples/Suspense.vue';

// Styles
import './style.css';
import 'overlayscrollbars/overlayscrollbars.css';

// Routes
const routes = [
    { path: '/', name: 'home', component: Home },
    { path: '/modules', name: 'modules', component: Modules },
    { path: '/suspense', name: 'suspense', component: Suspense }
];

// Router
const router = createRouter({
    history: createWebHistory(),
    routes,
});

// i18n
const messages = {
    en: {
        message: {
            hello: 'hello world'
        }
    },
    ja: {
        message: {
            hello: 'こんにちは、世界'
        }
    }
}

const i18n = createI18n({
    locale: 'ja',
    fallbackLocale: 'en',
    messages,
});

const app = createApp(App);

app.use(router);
app.use(i18n);
app.mount('#app');
