module.exports = {
    parserOptions: {
        ecmaVersion: 'latest',
        sourceType: 'module'
    },
    root: true,
    env: {
        node: true,
    },
    'extends': [
        'plugin:vue/strongly-recommended'
    ],
    rules: {
        'vue/max-attributes-per-line': 'off',
        'vue/singleline-html-element-content-newline': 'off',
        'indent': ['error', 4],
        'vue/script-indent': [
            'error',
            4,
            { 'baseIndent': 1 }
        ],
        'vue/html-indent': [
            'error',
            4,
            { 'baseIndent': 1 }
        ],
    },
    'overrides': [
        {
            'files': ['*.vue'],
            'rules': {
                'indent': 'off'
            }
        }
    ]
};