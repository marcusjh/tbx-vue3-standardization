# VUE3 Standard ways of working

## Changes to hook names

- Excluding `beforeCreate` and `created` (which are replaced by setup method itself), there are 9 options API lifecycle hooks that we can access in our setup method.

- `onBeforeMount` - Called before mounting begins.
- `onMounted` - Called when component is mounted.
- `onBeforeUpdate` - Called when reactive data changes and before re-render.
- `onUpdated` - Called after re-render.
- `onBeforeUnmount` - Called before the vue instance is destroyed.
- `onUnmounted` - Called after the instance is destroyed.
- `onActivated` - Called when a kept-alive component is activated.
- `onDeactivated` - Called when a kept-alive component is deactivated.
- `onErrorCaptured` - Called when an error is captured from a child component.

## Updating from VUE2 options API to VUE3 composition API lifecycle hooks

- `beforeCreate` -> `setup()`
- `created` -> `setup()`
- `beforeMount` -> `onBeforeMount`
- `mounted` -> `onMounted`
- `beforeUpdate` -> `onBeforeUpdate`
- `updated` -> `onUpdated`
- `beforeDestroy` -> `onBeforeUnmount`
- `destroyed` -> `onUnmounted`
- `errorCaptured` -> `onErrorCaptured`
