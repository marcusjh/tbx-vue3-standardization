module.exports = {
    root: true,
    parserOptions: {
      sourceType: "module"
    },
    extends: [
        "@nuxt/eslint-config",
        "@nuxtjs/eslint-config-typescript"
    ],
};
